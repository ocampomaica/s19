
// S19 - Javascript - Selection Control Structures

/*ACTIVITY*/


/*

	1. Declare 3 global variables without initialization called username,password and role.
	2. Create a login function which is able to prompt the user to provide their username, password and role.
		a.) use prompt() and update the username,password and role global variables with the prompt() returned values.
		b.) add an if statement to check if the username is an empty string or null or if the password is an empty string or null or if the role is an empty string or null.
			** if it is, show an alert to inform the user that their input should not be empty.
		c.) Add an else statement. Inside the else statement add a switch to check the user's role add 3 cases and a default:
			** if the user's role is admin, show an alert with the following message:
						"Welcome back to the class portal, admin!"
			** if the user's role is teacher, show an alert with the following message:
						"Thank you for logging in, teacher!"
			** if the user's role is a student, show an alert with the following message:
 						"Welcome to the class portal, student!"
			** if the user's role does not fall under any of the cases, as a default, show a message:
 						"Role out of range."
*/


	// Code here:
console.log("Maica")

let username;
let password;
let role;

function user(username,password,role) {
	username = prompt("Enter your username:").toLowerCase();
	password = prompt("Enter your password:").toLowerCase();
	role = prompt("Enter your role:").toLowerCase();

	if(username === "") {
		alert("The username should not be empty")
	}
	else if(password === "") {
		alert("The password should not be empty")
	}
	else if(role === "") {
		alert("The role should not be empty")
	}
	else {
		switch(role){
			case 'admin':
			console.log("Welcome back to the class portal, admin"+ ' ' + username);
			break;

			case 'teacher':
			console.log("Welcome back to the class portal, teacher" + ' ' + username);
			break;

			case 'student':
			console.log("Welcome back to the class portal, student" + ' ' + username);
			break;

			default:
			console.log("Role out of range!");
			break;
		}
		//console.log("Welcome back to the class portal, " + role + ' ' + username)
	}
}

user();



/*
	3. Create a function which is able to receive 4 numbers as arguments, calculate its average and log a message for  the user about their letter equivalent in the console.
		a.) add parameters appropriate to describe the arguments.
		b.)create a new function scoped variable called average.
		c.) calculate the average of the 4 number inputs and store it in the variable average.
		d.)research the use of Math.round() and round off the value of the average variable.
			**update the average variable with the use of Math.round()
			**console.log() the average variable to check if it is rounding off first.

	4. add an if statement to check if the value of avg is less than or equal to 74.
		a.) if it is, show the following message in a console.log():
			"Hello, student, your average is <show average>. The letter equivalent is F"

	5. add an else if statement to check if the value of avg is greater than or equal to 75 and if average is less than or equal to 79.
		a.) if it is, show the following message in a console.log():
			"Hello, student, your average is <show average>. The letter equivalent is D"

	6. add an else if statement to check if the value of avg is greater than or equal to 80 and if average is less than or equal to 84.
		a.) if it is, show the following message in a console.log():
			"Hello, student, your average is <show average>. The letter equivalent is C"

	7. add an else if statement to check if the value of avg is greater than or equal to 85 and if average is less than or equal to 89.
		a.) if it is, show the following message in a console.log():
			"Hello, student, your average is <show average>. The letter equivalent is B"

	8. add an else if statement to check if the value of avg is greater than or equal to 90 and if average is less than or equal to 95.
		a.) if it is, show the following message in a console.log():
			"Hello, student, your average is <show average>. The letter equivalent is A"

	9. add an else if statement to check if the value of average is greater than 96.
		a.) if it is, show the following message in a console.log():
			"Hello, student, your average is <show average>. The letter equivalent is A+"

*/


	// Code here:
function average(num1,num2,num3,num4) {
	let totalAverage = (num1 + num2 + num3 + num4) / 4;
	console.log("Scores: " + num1 + ', ' + num2 + ', ' + num3 + ', ' + num4);

	if(totalAverage<=74) {
		console.log("Hello, student! Your average is: " + Math.round(totalAverage) + ". The letter equivalent is F");
	}

	else if(totalAverage>=75 && totalAverage<=79) {
		console.log("Hello, student! Your average is: " + Math.round(totalAverage) + ". The letter equivalent is D");
	}

	else if(totalAverage>=80 && totalAverage<=84) {
		console.log("Hello, student! Your average is: " + Math.round(totalAverage) + ". The letter equivalent is C");
	}

	else if(totalAverage>=85 && totalAverage<=89) {
		console.log("Hello, student! Your average is: " + Math.round(totalAverage) + ". The letter equivalent is B");
	}

	else if(totalAverage>=90 && totalAverage<=95) {
		console.log("Hello, student! Your average is: " + Math.round(totalAverage) + ". The letter equivalent is A");
	}

	else if(totalAverage<=96) {
		console.log("Hello, student! Your average is: " + Math.round(totalAverage) + ". The letter equivalent is A+");
	}
	
}

let totalAve = average(65,85,50,70);
console.log(totalAve);

totalAve = average(75,79,78,77);
console.log(totalAve);

totalAve = average(80,84,82,81);
console.log(totalAve);

totalAve = average(89,85,88,86);
console.log(totalAve);

totalAve = average(95,90,93,94);
console.log(totalAve);

totalAve = average(99,98,96,90);
console.log(totalAve);